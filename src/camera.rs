use crate::{obstacle::Obstacle, ray::Ray, space::Space};
use image::io::Reader as ImageReader;
use image::{GenericImageView, ImageBuffer, Pixel, Rgb, RgbImage};
use ndarray::Array1;
use num_integer::Roots;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::f64::consts::PI;
use std::sync::atomic::AtomicUsize;

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Camera {
    pub position: Array1<f64>,    // r, theta, phi
    pub orientation: Array1<f64>, // theta, phi, psi
    pub im_size: [u32; 2],
    pub fov: [f64; 2],
    pub background_image_feed: String,
    pub background_image_feed_2: String,
}
#[derive(Debug, Default, Clone, PartialEq)]
pub struct IntegrationParameters {
    pub max_steps: usize,
    pub step_size: f64,
    pub use_adaptive_step: bool,
    pub sphere_invariance_opimization: bool,
}

pub fn rotate_to_initial_plane(collision_angle: [f64; 2], phi: f64, camera: &Camera) -> [f64; 2] {
    let mut x = /*collision_angle[0].sin()* */ collision_angle[1].cos();
    let mut y = /*collision_angle[0].sin() * */ collision_angle[1].sin();
    let mut z: f64 = 0.; //collision_angle[0].cos();

    // Rotation X matrix --> Correct 'phi' tilt (camera z rotation)
    let y2 = y * (phi.cos()) - z * (phi.sin());
    let z2 = y * (phi.sin()) + z * (phi.cos());
    y = y2;
    z = z2;

    // Rotation Y matrix --> Correct camera 'theta' tilt (camera x rotation)
    let x3 =
        x * ((camera.position[1] - PI / 2.).cos()) + z * ((camera.position[1] - PI / 2.).sin());
    let z3 =
        -x * ((camera.position[1] - PI / 2.).sin()) + z * ((camera.position[1] - PI / 2.).cos());
    x = x3;
    z = z3;

    // Back to spherical coordinates
    let mut new_point = collision_angle;
    new_point[0] = z.acos();
    new_point[1] = y.atan2(x);

    // Camera position phi (right ascension)
    new_point[1] += camera.position[2];
    while new_point[1] < 0. {
        new_point[1] += 2. * PI;
    }
    while new_point[1] >= 2. * PI {
        new_point[1] -= 2. * PI;
    }
    new_point
}

impl Camera {
    pub fn new() -> Self {
        Camera {
            position: Array1::<f64>::zeros(3),
            orientation: Array1::<f64>::zeros(3),
            im_size: [100, 100],
            fov: [PI / 4.; 2],
            background_image_feed: String::new(),
            background_image_feed_2: String::new(),
        }
    }

    pub fn render(
        &self,
        n_rays: usize,
        parameters: IntegrationParameters,
        space: &mut Space,
        exposition: f64,
        gamma: f64,
        save_file: &str,
    ) {
        // If background image is provided, call the alternate 'render_with_panorama' function
        if !self.background_image_feed.is_empty() || !self.background_image_feed_2.is_empty() {
            self.render_with_panorama(n_rays, parameters, space, exposition, gamma, save_file);
            return;
        }
        // Prepare parameters
        let n_rays = n_rays.sqrt();
        let n_rays_float = n_rays as f64;
        let size_x = self.im_size[0];
        let size_x_float = size_x as f64;
        let size_y = self.im_size[1];
        let size_y_float = size_y as f64;
        let mut img: RgbImage = ImageBuffer::new(self.im_size[0], self.im_size[1]);
        let coordinates: Vec<(u32, u32)> = img
            .enumerate_pixels()
            .into_iter()
            .map(|(x, y, _)| (x, y))
            .collect();
        let n_steps = coordinates.len();
        let progression = AtomicUsize::new(0);
        // Multi-threaded ray marching computing r g b values for each pixel
        let vec_pixels: Vec<Rgb<f64>> = coordinates
            .into_par_iter()
            .map(|(x, y)| {
                let mut r: f64 = 0.;
                let mut g: f64 = 0.;
                let mut b: f64 = 0.;
                for ray_x in 0..n_rays {
                    for ray_y in 0..n_rays {
                        // Pixel coordinate map to view X & Y angles (cx & cy)
                        let cx = ((x as f64) - (size_x_float - 1.) / 2.) * self.fov[0]
                            / size_x_float
                            + ((ray_x as f64) - (n_rays_float - 1.) / 2.) * self.fov[0]
                                / (size_x_float * n_rays_float)
                            - self.orientation[1];
                        let cy = ((y as f64) - (size_y_float - 1.) / 2.) * self.fov[1]
                            / size_y_float
                            + ((ray_y as f64) - (n_rays_float - 1.) / 2.) * self.fov[1]
                                / (size_y_float * n_rays_float)
                            - self.orientation[0];
                        // View angles --> (pixels') polar coordinates on the image plane
                        let theta = (cx.powi(2) + cy.powi(2)).sqrt();
                        let phi = PI / 2. - self.orientation[2] + cy.atan2(cx);
                        // Check for singularities in initialization (--> causes NaN rgb value)
                        if theta == 0. {
                            println!(
                                "Zero Angle Theta: for i = {}, j = {} are {},{}",
                                x, y, ray_x, ray_y
                            );
                        }
                        // Initialize ray sample
                        let mut ray_position = Array1::<f64>::zeros(4);
                        let mut ray_orientation = Array1::<f64>::zeros(2);
                        // If this optimization is enabled, all rays are integrated on equator plane Theta=PI/2
                        // then rotated back to original angle later. If the manifold is spherically invariant,
                        // this can be used to avoid coordinate singularities around theta = 0 & theta = PI.
                        if parameters.sphere_invariance_opimization {
                            ray_position[1] = self.position[0];
                            ray_position[2] = PI / 2.;
                            ray_position[3] = 0.;
                            ray_orientation[0] = theta;
                            ray_orientation[1] = PI / 2.;
                        } else {
                            ray_position[1] = self.position[0];
                            ray_position[2] = self.position[1];
                            ray_position[3] = self.position[2];
                            ray_orientation[0] = theta;
                            ray_orientation[1] = phi;
                        }

                        let mut ray = Ray::new_i(
                            -parameters.step_size,
                            &ray_position,
                            &ray_orientation,
                            1.,
                            space,
                        );
                        // Integration happens backwards, dLambda step is negative
                        let d_lambda = -parameters.step_size;
                        // Integrate path
                        let result_trace = ray.trace(
                            space,
                            parameters.max_steps,
                            d_lambda,
                            parameters.use_adaptive_step,
                            false,
                        );
                        // Manage collisions
                        if let Some(collision) = result_trace {
                            if matches!(
                                collision.collision_obstacle,
                                Obstacle::MaxDistancePanorama2 { r: _ }
                            ) {
                                // This 'render' function does not support background images
                                r += 255.;
                                g += 0.;
                                b += 255.;
                            }
                            let rgb = collision.color;
                            r += rgb[0] as f64;
                            g += rgb[1] as f64;
                            b += rgb[2] as f64;
                        } else if f64::is_nan(ray.position[1]) {
                            r += 1.; //255.;
                            g += 0.;
                            b += 0.;
                        } else {
                            r += 0.;
                            g += 0.;
                            b += 0.; //255.;
                        }
                    }
                }
                let progression_counter =
                    progression.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                if progression_counter % (n_steps / 10) == 0 {
                    println!(
                        "Progression : {:?} %",
                        (progression_counter as f64 / (n_steps as f64)) * 100.,
                    );
                }
                image::Rgb([r, g, b])
            })
            .collect();
        // Compute normalization values
        let max_value: f64 = vec_pixels
            .iter()
            .map(|pixel| pixel[0].max(pixel[1]).max(pixel[2]))
            .reduce(f64::max)
            .expect("This iterator is not empty");
        // Normalize image
        for ((_, _, pixel_img), pixel_calculated) in img.enumerate_pixels_mut().zip(vec_pixels) {
            let r =
                ((pixel_calculated[0] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            let g =
                ((pixel_calculated[1] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            let b =
                ((pixel_calculated[2] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            *pixel_img = Rgb::from([r, g, b]);
        }
        // Save image
        img.save(save_file).expect("Problem on saving image");
    }

    fn render_with_panorama(
        &self,
        n_rays: usize,
        parameters: IntegrationParameters,
        space: &mut Space,
        exposition: f64,
        gamma: f64,
        save_file: &str,
    ) {
        // Load background image(s)
        let background_image_1 = ImageReader::open(&self.background_image_feed)
            .expect("Error when loading background image(s)")
            .decode()
            .expect("Error decoding");
        let background_image_2 = ImageReader::open(&self.background_image_feed_2)
            .expect("Error when loading background image(s) n°2")
            .decode()
            .expect("Error decoding image n°2");
        // Prepare parameters
        let n_rays = n_rays.sqrt();
        let n_rays_float = n_rays as f64;
        let size_x = self.im_size[0];
        let size_x_float = size_x as f64;
        let size_y = self.im_size[1];
        let size_y_float = size_y as f64;
        let mut img: RgbImage = ImageBuffer::new(self.im_size[0], self.im_size[1]);
        let coordinates: Vec<(u32, u32)> = img
            .enumerate_pixels()
            .into_iter()
            .map(|(x, y, _)| (x, y))
            .collect();
        let n_steps = coordinates.len();
        let progression = AtomicUsize::new(0);
        // Multi-threaded ray marching computing r g b values for each pixel
        let vec_pixels: Vec<Rgb<f64>> = coordinates
            .into_par_iter()
            .map(|(x, y)| {
                let mut r: f64 = 0.;
                let mut g: f64 = 0.;
                let mut b: f64 = 0.;
                for ray_x in 0..n_rays {
                    for ray_y in 0..n_rays {
                        // Pixel coordinate map to view X & Y angles (cx & cy)
                        let cx = ((x as f64) - (size_x_float - 1.) / 2.) * self.fov[0]
                            / size_x_float
                            + ((ray_x as f64) - (n_rays_float - 1.) / 2.) * self.fov[0]
                                / (size_x_float * n_rays_float)
                            - self.orientation[1];
                        let cy = ((y as f64) - (size_y_float - 1.) / 2.) * self.fov[1]
                            / size_y_float
                            + ((ray_y as f64) - (n_rays_float - 1.) / 2.) * self.fov[1]
                                / (size_y_float * n_rays_float)
                            - self.orientation[0];
                        // View angles --> (pixels') polar coordinates on the image plane
                        let theta = (cx.powi(2) + cy.powi(2)).sqrt();
                        let phi = PI / 2. - self.orientation[2] + cy.atan2(cx);
                        // Check for singularities in initialization (--> causes NaN rgb value)
                        if theta == 0. {
                            println!(
                                "Zero Angle Theta: for i = {}, j = {} are {},{}",
                                x, y, ray_x, ray_y
                            );
                        }
                        // Initialize ray sample
                        let mut ray_position = Array1::<f64>::zeros(4);
                        let mut ray_orientation = Array1::<f64>::zeros(2);
                        // If this optimization is enabled, all rays are integrated on equator plane Theta=PI/2
                        // then rotated back to original angle later. If the manifold is spherically invariant,
                        // this can be used to avoid coordinate singularities around theta = 0 & theta = PI.
                        if parameters.sphere_invariance_opimization {
                            ray_position[1] = self.position[0];
                            ray_position[2] = PI / 2.;
                            ray_position[3] = 0.;
                            ray_orientation[0] = theta;
                            ray_orientation[1] = PI / 2.;
                        } else {
                            ray_position[1] = self.position[0];
                            ray_position[2] = self.position[1];
                            ray_position[3] = self.position[2];
                            ray_orientation[0] = theta;
                            ray_orientation[1] = phi;
                        }

                        let mut ray = Ray::new_i(
                            -parameters.step_size,
                            &ray_position,
                            &ray_orientation,
                            1.,
                            space,
                        );
                        // Integration happens backwards, dLambda step is negative
                        let d_lambda = -parameters.step_size;
                        // Integrate path
                        let result_trace =
                            ray.trace(space, parameters.max_steps, d_lambda, true, false);
                        // Manage collisions
                        if let Some(collision) = result_trace {
                            if matches!(
                                collision.collision_obstacle,
                                Obstacle::MaxDistancePanorama2 { r: _ }
                            ) {
                                // Retrieves asymptotic coordinates encoded in color
                                let mut rtp = collision.color;
                                // If optimization enabled, rotate coordinates back to initial angle
                                if parameters.sphere_invariance_opimization {
                                    let rotated = rotate_to_initial_plane(
                                        [PI / 2., rtp[2] * 2. * PI],
                                        phi - PI / 2.,
                                        self,
                                    );
                                    rtp[1] = rotated[0] / PI;
                                    rtp[2] = rotated[1] / PI / 2.;
                                }
                                /*
                                let mut theta1 = axy[1] * PI;
                                let mut phi1 = axy[2] * 2. * PI;
                                //println!("Theta {} Phi {}", theta1, phi1);
                                let x1 = 1. * theta1.sin() * phi1.cos();
                                let mut y1 = 1. * theta1.sin() * phi1.sin();
                                let mut z1 = 1. * theta1.cos();
                                let angle = phi - PI/2.;
                                let y2 = y1 * angle.cos() - z1 * angle.sin();
                                let z2 = y1 * angle.sin() + z1 * angle.cos();
                                let mut theta2 = (z2/1.).acos();
                                let mut phi2 = y2.atan2(x1);

                                if phi2 < 0. {
                                    phi2 += 2. * PI;
                                }
                                if phi2 >= 2. * PI {
                                    phi2 -= 2. * PI;
                                }
                                /*
                                if (theta2- theta1).abs() > 0.001 {
                                    println!("Erreur Theta1 {} != theta2 {} ; z1 = {}", theta1, theta2, z1);
                                }
                                if (phi2 - phi1).abs() > 0.001 {
                                    println!("Erreur Phi1 {} != Phi2 {}", phi1, phi2);
                                }*/
                                theta1 = theta2;
                                phi1 = phi2;
                                axy[1] = theta1 / PI;
                                axy[2] = phi1 / 2. / PI;*/

                                // (Wormholes) If first element is +1, asymptotic position is in first universe
                                // If it is equal to -1, asymptotic position is in second universe
                                if rtp[0] > 0. {
                                    // Read equirectangular image
                                    let pixel = background_image_1
                                        .get_pixel(
                                            (rtp[2] * background_image_1.width() as f64) as u32,
                                            (rtp[1] * background_image_1.height() as f64) as u32,
                                        )
                                        .to_rgb();
                                    r += pixel[0] as f64;
                                    g += pixel[1] as f64;
                                    b += pixel[2] as f64;
                                } else {
                                    // Read equirectangular image
                                    let pixel = background_image_2
                                        .get_pixel(
                                            (rtp[2] * background_image_2.width() as f64) as u32,
                                            (rtp[1] * background_image_2.height() as f64) as u32,
                                        )
                                        .to_rgb();
                                    r += pixel[0] as f64;
                                    g += pixel[1] as f64;
                                    b += pixel[2] as f64;
                                }
                                /*
                                r = (x1+1.)/2. *255.;
                                g = 0.;
                                b = 0.;//(y1+1.)/2. *255.;
                                if((phi1-PI/2.).abs() < 0.05 || (phi1- PI*3./2.).abs() < 0.05){
                                    r = 255.;
                                    g = 255.;
                                    b = 255.;
                                }
                                if(phi1.abs() < 0.05 || (phi1-2.*PI).abs() < 0.05 ){
                                    r = 50.;
                                    g = 255.;
                                    b = 50.;
                                }*/
                            } else {
                                let rgb = collision.color;
                                r += rgb[0] as f64;
                                g += rgb[1] as f64;
                                b += rgb[2] as f64;
                            }
                        } else if f64::is_nan(ray.position[1]) {
                            r += 1.; //255.;
                            g += 0.;
                            b += 0.;
                        } else {
                            r += 0.;
                            g += 0.;
                            b += 0.; //255.;
                        }
                    }
                }
                let progression_counter =
                    progression.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
                if progression_counter % (n_steps / 10) == 0 {
                    println!(
                        "Progression : {:?} %",
                        (progression_counter as f64 / (n_steps as f64)) * 100.,
                    );
                }
                image::Rgb([r, g, b])
            })
            .collect();
        // Compute normalization values
        let max_value: f64 = vec_pixels
            .iter()
            .map(|pixel| pixel[0].max(pixel[1]).max(pixel[2]))
            .reduce(f64::max)
            .expect("This iterator is not empty");
        // Normalize image
        for ((_, _, pixel_img), pixel_calculated) in img.enumerate_pixels_mut().zip(vec_pixels) {
            let r =
                ((pixel_calculated[0] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            let g =
                ((pixel_calculated[1] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            let b =
                ((pixel_calculated[2] / max_value * exposition).powf(gamma) * 255.).min(255.) as u8;
            *pixel_img = Rgb::from([r, g, b]);
        }
        // Save image
        img.save(save_file).expect("Problem on saving image");
    }
}

impl IntegrationParameters {
    pub fn default() -> Self {
        IntegrationParameters {
            max_steps: 1000,
            step_size: 50.,
            use_adaptive_step: true,
            sphere_invariance_opimization: false,
        }
    }
}
