use relativistic_ray_tracing::camera::Camera;

fn main() {
    let camera = Camera::new();
    println!("test camera {:?}", camera);
    // Construct a new RGB ImageBuffer with the specified width and height.
}
#[allow(non_snake_case)]
#[cfg(test)]
mod unit_tests {
    use std::f64::consts::PI;

    use image::{ImageBuffer, RgbImage};
    use ndarray::{Array1, Array3};
    use relativistic_ray_tracing::camera::{Camera, IntegrationParameters};
    use relativistic_ray_tracing::obstacle::Obstacle;
    use relativistic_ray_tracing::space::ManifoldType;
    use relativistic_ray_tracing::{ray::Ray, space::Space};

    #[test]
    fn ray_tracing() {
        let position = Array1::<f64>::ones(4).mapv(|elem| elem * 2.);
        println!("test {:?}", position);

        let blackhole_manifold = ManifoldType::Schwarzschild { rs: 1. };
        let espace = Space {
            c: 1.0,
            manifold: blackhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::new(),
        };

        let mut ray = Ray::new();

        println!("ray intialised {:?}", ray);
        println!("BEFORE : Space {:?}", espace);
        espace.update_christoffel(&position);
        println!("AFTER : and space {:?}", espace);
        println!("{:?}", espace.christoffel[[0, 0, 1]]);

        println!("\nTest trace() --> ray init. at (t, r, theta, phi) = (0, 6, PI/2, 0),\nwith velocity = 0 (and thus proper time speed = 1).");
        println!("=> r should decrease as the object falls into the black hole.");
        ray.position[1] = 6.;
        ray.position[2] = PI / 2.;
        ray.position_derivative[0] = 1.;
        ray.trace(&espace, 10, 1., true, false);
    }

    #[test]
    fn circular_orbit() {
        let blackhole_manifold = ManifoldType::Schwarzschild { rs: 1. };
        let space = Space {
            c: 1.0,
            manifold: blackhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::new(),
        };
        let mut position = Array1::<f64>::zeros(4);
        position[1] = 1.5;
        position[2] = PI / 2.;
        let mut orientation = Array1::<f64>::zeros(3);
        orientation[0] = PI / 2.;
        orientation[1] = PI / 2.;
        let initial_velocity = space.c; //(C.powf(2.) * space.rs / 2. / (position[1] - space.rs)).sqrt();

        let step_size = 0.4;
        let number_steps = 50;
        let mut ray = Ray::new_i(step_size, &position, &orientation, initial_velocity, &space);

        ray.trace(&space, number_steps, step_size, true, true);

        let error_margin = 1e-3;
        println!("\n\nTest initial position : {:?}", position);
        println!("Test final position : {:?}", ray.position);
        assert!(
            (ray.position[1] <= position[1] + error_margin)
                && (ray.position[1] >= position[1] - error_margin)
        );
    }

    #[test]
    fn backwards_orbit() {
        let blackhole_manifold = ManifoldType::Schwarzschild { rs: 1. };
        let space = Space {
            c: 1.0,
            manifold: blackhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::new(),
        };
        let mut position = Array1::<f64>::zeros(4);
        position[1] = 1.5;
        position[2] = PI / 2.;
        let mut orientation = Array1::<f64>::zeros(3);
        orientation[0] = PI / 2.;
        orientation[1] = -PI / 2.;
        let initial_velocity = space.c; //(C.powf(2.) * space.rs / 2. / (position[1] - space.rs)).sqrt();

        let step_size = -0.4;
        let number_steps = 50;
        let mut ray = Ray::new_i(step_size, &position, &orientation, initial_velocity, &space);

        ray.trace(&space, number_steps, step_size, true, true);

        let error_margin = 1e-3;
        println!("\n\nTest initial position : {:?}", position);
        println!("Test final position : {:?}", ray.position);
        assert!(
            (ray.position[1] <= position[1] + error_margin)
                && (ray.position[1] >= position[1] - error_margin)
        );
    }

    #[test]
    fn outward_escape() {
        let radius = 100.;
        let blackhole_manifold = ManifoldType::Schwarzschild { rs: radius };
        let space = Space {
            c: 1.0,
            manifold: blackhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::new(),
        };
        let mut position = Array1::<f64>::zeros(4);
        position[1] = 200.;
        position[2] = PI / 2.;
        let mut orientation = Array1::<f64>::zeros(3);
        orientation[0] = 0.;
        orientation[1] = 0.;
        let initial_velocity = space.c; // Escapes at light speed : it's a photon

        let step_size = 2.;
        let number_steps = 100;
        let mut ray = Ray::new_i(step_size, &position, &orientation, initial_velocity, &space);

        ray.trace(&space, number_steps, step_size, true, true);

        let momentum_conservation =
            -ray.position_derivative[0].powi(2) * (1. - radius / ray.position[1]) / space.c.powi(2)
                + ray.position_derivative[1].powi(2) / (1. - radius / ray.position[1])
                + (ray.position_derivative[2] * ray.position[1]).powi(2)
                + (ray.position_derivative[3] * ray.position[1] * ray.position[2].sin()).powi(2);

        let error_margin = 1e-3;
        println!("Test initial position : {:?}", position);
        println!("Test final position : {:?}", ray.position);
        assert!(
            (ray.position[1] > position[1])
                && ((ray.position[2] - position[2]).abs() <= error_margin)
                && ((ray.position[3] - position[3]).abs() <= error_margin)
                && (momentum_conservation.abs() <= error_margin)
        );
    }

    #[test]
    fn wormhole_traversal() {
        let radius = 100.;
        let wormhole_manifold = ManifoldType::MorrisThorne { b0: radius };
        let space = Space {
            c: 1.0,
            manifold: wormhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::new(),
        };
        let mut position = Array1::<f64>::zeros(4);
        position[1] = 500.;
        position[2] = PI / 2.;
        let mut orientation = Array1::<f64>::zeros(3);
        orientation[0] = PI;
        orientation[1] = 0.;
        let initial_velocity = space.c; // Moves at light speed : it's a photon

        let step_size = 10.;
        let number_steps = 100;
        let mut ray = Ray::new_i(step_size, &position, &orientation, initial_velocity, &space);

        ray.trace(&space, number_steps, step_size, true, true);

        let metric = space.metric(&ray.position);
        let momentum_conservation = ray.position_derivative[0].powi(2) * metric[0]
            + ray.position_derivative[1].powi(2) * metric[1]
            + ray.position_derivative[2].powi(2) * metric[2]
            + ray.position_derivative[3].powi(2) * metric[3];

        let error_margin = 1e-3;
        println!("Test initial position : {:?}", position);
        println!("Test final position : {:?}", ray.position);
        assert!(
            (ray.position[1] < 0.)
                && ((ray.position[2] - position[2]).abs() <= error_margin)
                && ((ray.position[3] - position[3]).abs() <= error_margin)
                && (momentum_conservation.abs() <= error_margin)
        );
    }

    #[test]
    fn test_image_plot() {
        let _result = match std::fs::remove_file("test.png") {
            Ok(()) => (),
            Err(e) => println!("Could not remove file because of : {:?}", e),
        };
        let _img: RgbImage = ImageBuffer::new(512, 512);

        // Construct a new by repeated calls to the supplied closure.
        let mut img = ImageBuffer::from_fn(512, 512, |x, _y| {
            if x % 2 == 0 {
                image::Luma([0u8])
            } else {
                image::Luma([255u8])
            }
        });

        // Obtain the image's width and height.
        let (_width, _height) = img.dimensions();

        // Access the pixel at coordinate (100, 100).
        let _pixel = img[(100, 100)];

        // Or use the `get_pixel` method from the `GenericImage` trait.
        let pixel = *img.get_pixel(100, 100);

        // Put a pixel at coordinate (100, 100).
        img.put_pixel(100, 100, pixel);

        // Create a new ImgBuf with width: imgx and height: imgy
        let mut imgbuf = image::ImageBuffer::new(1000, 1000);
        // Iterate over the coordinates and pixels of the image
        for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
            let r = (0.3 * x as f32) as u8;
            let b = (0.3 * y as f32) as u8;
            *pixel = image::Rgb([r, 0, b]);
        }
        // Save the image as “fractal.png”, the format is deduced from the path
        imgbuf.save("test.png").expect("Problem on saving image");
    }

    #[test]
    fn test_render() {
        let black_hole_radius = 100.;
        let camera_distance = 30. * black_hole_radius;
        let blackhole = Obstacle::BlackHole {
            r: black_hole_radius,
        };
        let accretionDisk = Obstacle::AccretionDisk {
            r_min: 3. * black_hole_radius,
            r_max: 20. * black_hole_radius,
            thickness: 1.,
            temperature: 2500.,
        };
        let max_radius = Obstacle::MaxDistance {
            r: camera_distance * 1.1,
        };
        let blackhole_manifold = ManifoldType::Schwarzschild { rs: 100. };
        let mut space = Space {
            c: 1.0,
            manifold: blackhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::from([blackhole, max_radius, accretionDisk]),
        };

        let mut cam_position = Array1::<f64>::zeros(3);
        cam_position[0] = camera_distance;
        cam_position[1] = PI * 0.455;
        let mut cam_orientation = Array1::<f64>::zeros(3);
        cam_orientation[0] = 0.;
        cam_orientation[1] = 0.;

        let camera = Camera {
            fov: [PI / 5., PI / 5.],
            im_size: [50, 50],
            orientation: cam_orientation,
            position: cam_position,
            background_image_feed: String::new(),
            background_image_feed_2: String::new(),
        };
        let parameters = IntegrationParameters {
            max_steps: 1000,
            step_size: 40.,
            use_adaptive_step: true,
            sphere_invariance_opimization: false,
        };
        camera.render(4, parameters, &mut space, 2.5, 0.75, "test_blackhole.png");
    }

    #[test]
    fn wormhole_render() {
        let worm_hole_radius = 100.;
        let camera_distance = 5. * worm_hole_radius;

        let max_radius = Obstacle::MaxDistancePanorama2 {
            r: camera_distance * 1.1,
        };
        let wormhole_manifold = ManifoldType::MorrisThorne {
            b0: worm_hole_radius,
        };
        let mut space = Space {
            c: 1.0,
            manifold: wormhole_manifold,
            christoffel: Array3::zeros((4, 4, 4)),
            obstacles: Vec::from([max_radius]),
            //obstacles: Vec::from([max_radius, accretionDisk]),
        };

        let mut cam_position = Array1::<f64>::zeros(3);
        cam_position[0] = camera_distance;
        cam_position[1] = PI * 0.5;
        cam_position[2] = PI * 0.5;
        let mut cam_orientation = Array1::<f64>::zeros(3);
        cam_orientation[0] = -0.2;
        cam_orientation[1] = 0.5;
        cam_orientation[2] = 0.3;

        let camera = Camera {
            fov: [PI / 2.5, PI / 5.],
            im_size: [100, 25],
            orientation: cam_orientation,
            position: cam_position,
            background_image_feed: "image1.jpg".to_string(),
            background_image_feed_2: "image2.jpg".to_string(),
        };
        let parameters = IntegrationParameters {
            max_steps: 4000,
            step_size: 40.,
            use_adaptive_step: true,
            sphere_invariance_opimization: true,
        };
        camera.render(1, parameters, &mut space, 1., 1., "test_wormhole.png");
    }
}
