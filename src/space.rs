use crate::obstacle::Obstacle;
use ndarray::{array, Array1, Array3};

#[derive(Debug, Clone, PartialEq)]
pub enum ManifoldType {
    Schwarzschild { rs: f64 },
    Kerr { rs: f64, a: f64 },
    MorrisThorne { b0: f64 },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Space {
    pub c: f64,
    pub manifold: ManifoldType,
    pub christoffel: Array3<f64>,
    pub obstacles: Vec<Obstacle>,
}

impl Space {
    pub fn update_christoffel(&self, position: &Array1<f64>) -> Space {
        let r = position[1];
        let theta = position[2];
        let mut updated_space = self.clone();

        match self.manifold {
            ManifoldType::Schwarzschild { rs } => {
                let a = 1. / (1. - (rs / r));
                let ap = -rs / ((r - rs).powi(2));
                let b = self.c * self.c * (rs / r - 1.);
                let bp = -self.c * self.c * rs / (r * r);

                // i = 0 : TIME
                updated_space.christoffel[[0, 0, 1]] = bp / (2. * b);
                updated_space.christoffel[[0, 1, 0]] = bp / (2. * b);

                // i = 1 : R
                updated_space.christoffel[[1, 0, 0]] = -bp / (2. * a);
                updated_space.christoffel[[1, 1, 1]] = ap / (2. * a);
                updated_space.christoffel[[1, 2, 2]] = -r / a;
                updated_space.christoffel[[1, 3, 3]] = -r * ((theta.sin()).powi(2)) / a;

                // i = 2 : Theta
                updated_space.christoffel[[2, 0, 0]] = 0.;
                updated_space.christoffel[[2, 1, 2]] = 1. / r;
                updated_space.christoffel[[2, 2, 1]] = 1. / r;
                updated_space.christoffel[[2, 3, 3]] = -(theta.sin()) * (theta.cos());
                // i = 3 : Phi
                updated_space.christoffel[[3, 1, 3]] = 1. / r;
                updated_space.christoffel[[3, 3, 1]] = 1. / r;
                updated_space.christoffel[[3, 2, 3]] = 1. / (theta.tan());
                updated_space.christoffel[[3, 3, 2]] = 1. / (theta.tan());
            }
            ManifoldType::Kerr { rs, a } => {
                let sigma = r * r + a * a * (theta.cos().powi(2));
                let delta = (r - rs) * r + a * a;
                let s2t = theta.sin().powi(2);
                let c2t = theta.cos().powi(2);
                let a2 = a * a;
                let r2 = r * r;

                // i = 0 : TIME
                updated_space.christoffel[[0, 0, 1]] =
                    rs * (r2 + a2) * (r2 - a2 * c2t) / (2. * sigma * sigma * delta);
                updated_space.christoffel[[0, 1, 0]] = updated_space.christoffel[[0, 0, 1]];
                updated_space.christoffel[[0, 0, 2]] =
                    -rs * a2 * r * theta.cos() * theta.sin() / (sigma * sigma);
                updated_space.christoffel[[0, 2, 0]] = updated_space.christoffel[[0, 0, 2]];
                updated_space.christoffel[[0, 1, 3]] =
                    rs * a * s2t * (a2 * c2t * (a2 - r2) - r2 * (a2 + 3. * r2))
                        / (2. * self.c * sigma * sigma * delta);
                updated_space.christoffel[[0, 3, 1]] = updated_space.christoffel[[0, 1, 3]];
                updated_space.christoffel[[0, 2, 3]] =
                    rs * a * a2 * r * theta.cos() * s2t * theta.sin() / (self.c * sigma * sigma);
                updated_space.christoffel[[0, 3, 2]] = updated_space.christoffel[[0, 0, 2]];

                // i = 1 : R
                updated_space.christoffel[[1, 0, 0]] =
                    self.c.powi(2) * rs * delta * (r2 - a2 * c2t) / (2. * sigma.powi(3));
                updated_space.christoffel[[1, 0, 3]] =
                    -self.c * delta * rs * a * s2t * (r2 - a2 * c2t) / (2. * sigma.powi(3));
                updated_space.christoffel[[1, 3, 0]] = updated_space.christoffel[[1, 0, 3]];
                updated_space.christoffel[[1, 1, 1]] =
                    (2. * r * a2 * s2t - rs * (r2 - a2 * c2t)) / (2. * sigma * delta);
                updated_space.christoffel[[1, 1, 2]] = -a2 * theta.sin() * theta.cos() / sigma;
                updated_space.christoffel[[1, 2, 1]] = updated_space.christoffel[[1, 1, 2]];
                updated_space.christoffel[[1, 2, 2]] = -r * delta / sigma;
                updated_space.christoffel[[1, 3, 3]] =
                    delta * s2t * (-2. * r * sigma * sigma + rs * a2 * s2t * (r2 - a2 * c2t))
                        / (2. * sigma.powi(3));

                // i = 2 : Theta
                updated_space.christoffel[[2, 0, 0]] =
                    -self.c.powi(2) * rs * a2 * r * theta.sin() * theta.cos() / sigma.powi(3);
                updated_space.christoffel[[2, 0, 3]] =
                    self.c * rs * a * r * (r2 + a2) * theta.sin() * theta.cos() / sigma.powi(3);
                updated_space.christoffel[[2, 3, 0]] = updated_space.christoffel[[2, 0, 3]];
                updated_space.christoffel[[2, 1, 1]] =
                    a2 * theta.sin() * theta.cos() / (sigma * delta);
                updated_space.christoffel[[2, 1, 2]] = r / sigma;
                updated_space.christoffel[[2, 2, 1]] = updated_space.christoffel[[2, 1, 2]];
                updated_space.christoffel[[2, 2, 2]] = -a2 * theta.sin() * theta.cos() / sigma;
                updated_space.christoffel[[2, 3, 3]] = -theta.sin()
                    * theta.cos()
                    * (((r2 + a2) * sigma + rs * a2 * r * s2t) * sigma
                        + (r2 + a2) * rs * a2 * r * s2t)
                    / sigma.powi(3);

                // i = 3 : Phi
                updated_space.christoffel[[3, 0, 1]] =
                    self.c * rs * a * (r2 - a2 * c2t) / (2. * sigma * sigma * delta);
                updated_space.christoffel[[3, 1, 0]] = updated_space.christoffel[[3, 0, 1]];
                updated_space.christoffel[[3, 0, 2]] =
                    -self.c * rs * a * r * theta.cos() / (theta.sin() * sigma * sigma);
                updated_space.christoffel[[3, 2, 0]] = updated_space.christoffel[[3, 0, 2]];
                updated_space.christoffel[[3, 1, 3]] = (2. * r * sigma * sigma
                    + rs * (a2 * a2 * s2t * c2t - r2 * (sigma + r2 + a2)))
                    / (2. * sigma * sigma * delta);
                updated_space.christoffel[[3, 3, 1]] = updated_space.christoffel[[3, 1, 3]];
                updated_space.christoffel[[3, 2, 3]] = (sigma * sigma + rs * a2 * r * s2t)
                    * theta.cos()
                    / (theta.sin() * sigma * sigma);
                updated_space.christoffel[[3, 3, 2]] = updated_space.christoffel[[3, 2, 3]];
            }
            ManifoldType::MorrisThorne { b0 } => {
                // i = 0 : TIME
                //updated_space.christoffel[[0, 0, 1]] = bp / (2. * b);
                //updated_space.christoffel[[0, 1, 0]] = bp / (2. * b);

                // i = 1 : R
                //updated_space.christoffel[[1, 0, 0]] = -bp / (2. * a);
                //updated_space.christoffel[[1, 1, 1]] = ap / (2. * a);
                updated_space.christoffel[[1, 2, 2]] = -r;
                updated_space.christoffel[[1, 3, 3]] = -r * ((theta.sin()).powi(2));

                // i = 2 : Theta
                //updated_space.christoffel[[2, 0, 0]] = 0.;
                updated_space.christoffel[[2, 1, 2]] = r / (b0.powi(2) + r.powi(2));
                updated_space.christoffel[[2, 2, 1]] = r / (b0.powi(2) + r.powi(2));
                updated_space.christoffel[[2, 3, 3]] = -(theta.sin()) * (theta.cos());
                // i = 3 : Phi
                updated_space.christoffel[[3, 1, 3]] = r / (b0.powi(2) + r.powi(2));
                updated_space.christoffel[[3, 3, 1]] = r / (b0.powi(2) + r.powi(2));
                updated_space.christoffel[[3, 2, 3]] = 1. / (theta.tan());
                updated_space.christoffel[[3, 3, 2]] = 1. / (theta.tan());
            }
        }
        updated_space
    }

    pub fn metric(&self, position: &Array1<f64>) -> [f64; 4] {
        match self.manifold {
            ManifoldType::Schwarzschild { rs } => [
                -(1. - rs / position[1]) / (self.c.powi(2)),
                1. / (1. - rs / position[1]),
                position[1].powi(2),
                (position[1] * (position[2].sin())).powi(2),
            ],
            ManifoldType::Kerr { rs: _, a: _ } => [0., 0., 0., 0.],
            ManifoldType::MorrisThorne { b0 } => [
                -self.c.powi(2),
                1.,
                position[1].powi(2) + b0.powi(2),
                (position[1].powi(2) + b0.powi(2)) * ((position[2].sin()).powi(2)),
            ],
        }
    }
    pub fn coordinates_to_spherical(&self, position: &Array1<f64>) -> Array1<f64> {
        match self.manifold {
            ManifoldType::Schwarzschild { rs: _ } => {
                array![position[0], position[1], position[2], position[3]]
            }
            ManifoldType::Kerr { rs: _, a: _ } => {
                array![position[0], position[1], position[2], position[3]]
            } //WRONG !!!
            ManifoldType::MorrisThorne { b0 } => array![
                position[0],
                (position[1].powi(2) + b0.powi(2)).sqrt() * (position[1].signum()),
                position[2],
                position[3],
            ],
        }
    }
    pub fn spherical_to_coordinates(&self, position: &Array1<f64>) -> Array1<f64> {
        match self.manifold {
            ManifoldType::Schwarzschild { rs: _ } => {
                array![position[0], position[1], position[2], position[3]]
            }
            ManifoldType::Kerr { rs: _, a: _ } => {
                array![position[0], position[1], position[2], position[3]]
            } //WRONG !!!
            ManifoldType::MorrisThorne { b0 } => array![
                position[0],
                (position[1].powi(2) - b0.powi(2)).sqrt() * (position[1].signum()),
                position[2],
                position[3],
            ],
        }
    }
}
